package prova;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

public class ProvaA{

	
	public static void main(String[] args) {
		
		String inputA = "abcde"; 
		String inputB = "abcbe";
		int [] ratings = {8,6,9,6,8};
		
		System.out.println(isUnique(inputB));
		System.out.println(candy(ratings));
		

	}

	
	public static boolean isUnique(String input) {
		ArrayList<Object>  list = new ArrayList<Object>();
		int sizeString = input.length();
		for (int i = 0; i < sizeString; i++) {
			char uniqueValue = input.charAt(i);
			if(!list.contains(uniqueValue)){
				list.add(uniqueValue);
			}else{
				return false;
			}
		}
		return true;
	}

	public static int candy(int[] ratings){
		HashMap<Object, Object> hash = new HashMap<Object, Object>();
		int qtdCandy = 1;
		int qtdMinCandy = 0;
		
		Arrays.sort(ratings);
		
		for (int i = 0; i < ratings.length; i++) {
			if(!hash.containsKey(ratings[i])){
				hash.put(ratings[i], qtdCandy++);
				qtdMinCandy = qtdMinCandy + Integer.valueOf(hash.get(ratings[i]).toString());
			}else{
				hash.put(ratings[i], hash.get(ratings[i]));
				qtdMinCandy = qtdMinCandy + Integer.valueOf(hash.get(ratings[i]).toString());
			}
		}
		return qtdMinCandy;
	}
}
