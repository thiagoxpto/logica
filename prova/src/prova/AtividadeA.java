package prova;

import java.util.ArrayList;

/**
 * 
 * @author thiago.tavares
 * Crie uma classe que implemente a interface Solution e o m�todo isUnique, 
 * o qual determina se uma string cont�m todos os elementos �nicos. 
 */
public class AtividadeA implements Solution{

	@Override
	public boolean isUnique(String input) {
		ArrayList<Object>  list = new ArrayList<Object>();
		int sizeString = input.length();
		for (int i = 0; i < sizeString; i++) {
			char uniqueValue = input.charAt(i);
			if(!list.contains(uniqueValue)){
				list.add(uniqueValue);
			}else{
				return false;
			}
		}
		return true;
	}

	@Override
	public int candy(int[] ratings) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ListNode removeNthFromEnd(ListNode head, int n) {
		// TODO Auto-generated method stub
		return null;
	}
}
