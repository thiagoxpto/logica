package prova;

public class ListaSimplementeEncadeada {

	//precisaremos do head(Cabe�a) e size(tamanho) , valor e next -> proximo
	
	protected Node head;
	protected long size;
	
	
	public ListaSimplementeEncadeada() {
		this.head = null;
		this.size = 0;
	}
	
	public long size(){
		return size;
	}
	
	public boolean isEmpty(){
		return size == 0;
	}
	
	/**
	 * Insere o elemento na cabeca ->  no inicio.
	 * @param r
	 */
	public void inserirNaCabeca(Resultado r){
		Node v = new Node();
		v.setValor(r);
		v.setNext(head);
		
		head = v;
		size = size + 1;
	}
	
	public void inserirNaCalda(Resultado r){
		if(head == null){   //verificando se n�o existe nenhum elemento na lista simplesmente encadeado.
			inserirNaCabeca(r);
		}else{
			Node p = head;
			
			while(p.getNext() != null){
				p = p.getNext();
			}
				Node v = new Node();
				v.setValor(r);
				v.setNext(null);
				
				p.setNext(v);
				size = size + 1;
		}
	}
	
	
	
}
