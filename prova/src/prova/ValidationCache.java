package prova;

import java.util.HashMap;

/**
 * @author thiago.tavares
 * Implementar uma estrutura de dados para Least Recently Used (LRU) cache.  
 *
 */
public class ValidationCache implements LRUCache {

    HashMap<Integer, ValidationCache> map = new HashMap<Integer, ValidationCache>();
    private static final int CAPACITY = 5;
    private int key;
    private int value;
    private ValidationCache pre = null;
    private ValidationCache next = null;
    private ValidationCache head = null;
    private ValidationCache lastCacheNoUtilized = null;
    
    public ValidationCache() {
	}
    
    public ValidationCache(int key, int value){
        this.key = key;
        this.value = value;
    }
	
    @Override
	public int get(int key) {
		if(map.containsKey(key)){
		 	ValidationCache n = map.get(key);
            removeCache(n);
            setHeadCache(n);
            return n.value;
	    }
	    return -1;
	}
    
    @Override
    public void set(int key, int value) {
        if(map.containsKey(key)){
        	ValidationCache oldVal = map.get(key);
        	oldVal.value = value;
            removeCache(oldVal);
            setHeadCache(oldVal);
        }else{
        	ValidationCache created = new ValidationCache(key, value);
            if(map.size() >= CAPACITY){  
                map.remove(lastCacheNoUtilized.key);
                removeCache(lastCacheNoUtilized);
                setHeadCache(created);
            }else{
            	setHeadCache(created);  
            }    
            map.put(key, created);
        }
    }
	
    /**
     *Removendo o cache nao utilizado
     * @param validationCache
     */
    public void removeCache(ValidationCache validationCache){
        if(validationCache.pre!=null){
        	validationCache.pre.next = validationCache.next;
        }else{
            head = validationCache.next;
        }
        if(validationCache.next!=null){
        	validationCache.next.pre = validationCache.pre;
        }else{
        	lastCacheNoUtilized = validationCache.pre;
        }
    }
	 
    /**
     * Definindo os atributos do cache
     * @param validationCache
     */
    public void setHeadCache(ValidationCache validationCache){
    	validationCache.next = head;
    	validationCache.pre = null;
        if(head!=null){
            head.pre = validationCache;
        }
        head = validationCache;
        if(lastCacheNoUtilized == null){
        	lastCacheNoUtilized = head;
        }
    }
}
