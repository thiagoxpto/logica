package prova;

import java.util.HashMap;


public class AtividadeC implements LRUCache {

    HashMap<Integer, AtividadeC> map = new HashMap<Integer, AtividadeC>();
    private static final int CAPACITY = 5;
    private int key;
    private int value;
    private AtividadeC pre = null;
    private AtividadeC next = null;
    private AtividadeC head = null;
    private AtividadeC lastCacheNoUtilized = null;
    
    public AtividadeC() {
	}
    
    public AtividadeC(int key, int value){
        this.key = key;
        this.value = value;
    }
	
    @Override
	public int get(int key) {
		if(map.containsKey(key)){
		 	AtividadeC n = map.get(key);
            removeCache(n);
            setHeadCache(n);
            return n.value;
	    }
	    return -1;
	}
    
    @Override
    public void set(int key, int value) {
        if(map.containsKey(key)){
        	AtividadeC oldVal = map.get(key);
        	oldVal.value = value;
            removeCache(oldVal);
            setHeadCache(oldVal);
        }else{
        	AtividadeC created = new AtividadeC(key, value);
            if(map.size() >= CAPACITY){  
                map.remove(lastCacheNoUtilized.key);
                removeCache(lastCacheNoUtilized);
                setHeadCache(created);
            }else{
            	setHeadCache(created);  
            }    
            map.put(key, created);
        }
    }
	
    /**
     *Removendo o cache nao utilizado
     * @param AtividadeC
     */
    public void removeCache(AtividadeC atividadeC){
        if(atividadeC.pre!=null){
        	atividadeC.pre.next = atividadeC.next;
        }else{
            head = atividadeC.next;
        }
        if(atividadeC.next!=null){
        	atividadeC.next.pre = atividadeC.pre;
        }else{
        	lastCacheNoUtilized = atividadeC.pre;
        }
    }
	 
    /**
     * Definindo os atributos do cache
     * @param AtividadeC
     */
    public void setHeadCache(AtividadeC atividadeC){
    	atividadeC.next = head;
    	atividadeC.pre = null;
        if(head!=null){
            head.pre = atividadeC;
        }
        head = atividadeC;
        if(lastCacheNoUtilized == null){
        	lastCacheNoUtilized = head;
        }
    }
}
