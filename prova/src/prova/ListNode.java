package prova;

import java.util.LinkedList;

/**
 * @author thiago.tavares
 * Dado uma LinkedList, remova o en�simo n� a partir do final da lista e retorne o head. 
 */
public class ListNode implements Solution{
	 LinkedList<ListNode> linkedList = new LinkedList<ListNode>();
	 int val;
	 ListNode next;
	 
	 ListNode(int x) {
		 val = x;
	 }

	@Override
	public ListNode removeNthFromEnd(ListNode head, int n) {
		linkedList.add(head);
		while (head.next != null) {
			head = head.next;
			linkedList.add(head.next);
		}
		if(n == linkedList.size()){
			linkedList.remove(linkedList.get(0));
		}else if(linkedList.size() > n){
			linkedList.get(linkedList.size() - (n + 1)).next = linkedList.get(linkedList.size() - (n-1));
			linkedList.remove(linkedList.size() - n);
		}else{
			return new ListNode(0);
		}
		return linkedList.peekFirst();
	}

	@Override
	public boolean isUnique(String input) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int candy(int[] ratings) {
		// TODO Auto-generated method stub
		return 0;
	}
}
