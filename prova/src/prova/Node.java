package prova;

/**
 * 
 * @author thiago.tavares
 * No
 */
public class Node {

	private Resultado valor;
	private Node next;
	
	public Resultado getValor() {
		return valor;
	}
	public void setValor(Resultado valor) {
		this.valor = valor;
	}
	public Node getNext() {
		return next;
	}
	public void setNext(Node next) {
		this.next = next;
	}

	
}
