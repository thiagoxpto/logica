package prova;

public interface Solution {

   boolean isUnique(String input);
   int candy(int[] ratings);
   
   ListNode removeNthFromEnd(ListNode head, int n);
   
}
