package prova;

import java.util.Arrays;
import java.util.HashMap;

/**
 * @author thiago.tavares
 * A professora em uma sala de aula decide presentear cada crian�a com doces conforme a sua nota.
 */
public class AtividadeB implements Solution{
	
	@Override
	public int candy(int[] ratings){
		HashMap<Object, Object> hash = new HashMap<Object, Object>();
		int qtdCandy = 1;
		int qtdMinCandy = 0;
		
		Arrays.sort(ratings);
		
		for (int i = 0; i < ratings.length; i++) {
			if(!hash.containsKey(ratings[i])){
				hash.put(ratings[i], qtdCandy++);
				qtdMinCandy = qtdMinCandy + Integer.valueOf(hash.get(ratings[i]).toString());
			}else{
				hash.put(ratings[i], hash.get(ratings[i]));
				qtdMinCandy = qtdMinCandy + Integer.valueOf(hash.get(ratings[i]).toString());
			}
		}
		return qtdMinCandy;
	}

	@Override
	public boolean isUnique(String input) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ListNode removeNthFromEnd(ListNode head, int n) {
		// TODO Auto-generated method stub
		return null;
	}
}
