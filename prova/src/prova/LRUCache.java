package prova;

public interface LRUCache {
  int get(final int key);
  void set(int key, int value);
}
