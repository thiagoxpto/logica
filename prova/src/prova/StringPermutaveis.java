package prova;

import java.util.Arrays;

/**
 * @author thiago.tavares
 * Verificando se os elementos da string a � igual a string b.  
 * Ex: string a = abc  � igual a string b = bca  return true; 
 */
public class StringPermutaveis {
	public static void main(String[] args) {
		String  valorA = "abccca";
	    String  valorB = "abccca";
	    System.out.println(stringPermutavel(valorA, valorB));
	}

	private static boolean stringPermutavel(String valorA, String valorB) {
		char[] listA = valorA.toCharArray();
		char[] listB = valorB.toCharArray();
	
		Arrays.sort(listA);
		Arrays.sort(listB);
	
		if(!Arrays.equals(listA, listB)){
			return false;	
		}
		return true;
	}
}
