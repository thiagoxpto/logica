package prova;

public class Fibonacci {

	public static void main(String[] args) {
	
		for (int i = 0; i < 30; i++) {
			System.out.print(fibo(i)+"\t");
		}
	}
	
	static long fibo(int n) { 
		int atual = 0; 
		int ant = 0; 
		 
		for (int i = 1; i <= n; i++) { 
			if (i == 1) { 
				atual = 1; 
				ant = 0; 
			} else { 
				atual += ant; 
				ant = atual - ant; 
			} 
		} 
		return atual;
	}
}
