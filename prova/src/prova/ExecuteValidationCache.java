package prova;

public class ExecuteValidationCache {

	public static void main(String[] args){
        int newVal;
		ValidationCache validationCache = new ValidationCache();
		validationCache.set(1, 1);
		validationCache.set(2, 2);
		validationCache.set(3, 3);
		validationCache.set(4, 4);
		validationCache.set(5, 5);
		
		newVal = validationCache.get(1);
		System.out.println("Utilizando o primeiro valor da lista: "+newVal);
		
		validationCache.set(6, 6); 
		
		int val2 = validationCache.get(2);
		System.out.println("Imprimindo o ultimo valor do cache: "+val2);
	}
}
